# Example of using MS561101ba03 driver

This is an example project for how to use the MS561101ba03 driver component
with the ESP32 ESP-IDF.

Export your IDF_PATH and toolchain path as usual e.g.

```sh
$ export $IDF_PATH=$HOME/path/to/esp-idf
$ export PATH="$PATH:$HOME/path/to/xtensa-esp32-elf/bin"
```

Clone this repository with:

```sh
$ git clone --recursive https://gitlab.com/mrnice/esp-idf-ms561101ba03-example.git
```

Configure the sdkconfig with:

```sh
$ make menuconfig
```

Then you can use

```sh
$ make flash
```
To flash the example to your ESP32.

The example output should be something like:

```sh
$ socat /dev/ttyUSB0,b115200 STDOUT
Temperature in C * 100: 2332 
Pressure in mbar * 100: 96724
...
```
